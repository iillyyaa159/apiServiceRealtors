// Register all the services here
const advertRepositoryFactory = require('./advertRepository');
const metroRepositoryFactory = require('./metroRepository');
const cityRepositoryFactory = require('./cityRepository');

module.exports = (db) => {
    const advertRepository = advertRepositoryFactory.create(db);
    const metroRepository = metroRepositoryFactory.create(db);
    const cityRepository = cityRepositoryFactory.create(db);
  return ({
      advertRepository,
      metroRepository,
      cityRepository
  });
};