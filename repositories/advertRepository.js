function create({ Advert }) {
    async function getAll() {
        const adverts = await Advert.findAll();
        return adverts.map(advert => advert.toAdvertModel());
    }

    async function get(id_advert) {
        const adverts = await Advert.findAll({
            where: {
                id_advert: id_advert
            }
        });
        return adverts.map(advert => advert.toAdvertModel());
    }

    async function add(advert) {
        await Advert.build(advert).save();
    }

    async function upsert(advert) {
        await Advert.upsert(advert);
    }

    async function remove(advert) {
        await Advert.build(advert).destroy();
    }

    return {
        add,
        getAll,
        upsert,
        remove,
        get,
    };
}

module.exports.create = create;
