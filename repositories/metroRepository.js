function create({ Metro }) {
    async function getAll() {
        const metros = await Metro.findAll();
        return metros.map(metro => metro.toMetroModel());
    }

    async function add(metro) {
        await Metro.build(metro).save();
    }

    async function upsert(metro) {
        await Metro.upsert(metro);
    }

    async function remove(metro) {
        await Metro.build(metro).destroy();
    }

    return {
        add,
        getAll,
        upsert,
        remove,
    };
}

module.exports.create = create;
