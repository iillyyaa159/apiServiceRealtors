function create({ City }) {
    async function getAll() {
        const citys = await City.findAll();
        return citys.map(city => city.toCityModel());
    }

    async function add(city) {
        await City.build(city).save();
    }

    async function upsert(city) {
        await City.upsert(city);
    }

    async function remove(city) {
        await City.build(city).destroy();
    }

    return {
        add,
        getAll,
        upsert,
        remove,
    };
}

module.exports.create = create;
