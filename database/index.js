const Sequelize = require('sequelize');
const { connectionString } = require('../configuration');

const sequelize = new Sequelize(connectionString, {
    logging: false
});

const Advert = require('./entities/Advert')(sequelize);
const City = require('./entities/City')(sequelize);
const Metro = require('./entities/Metro')(sequelize);

sequelize.sync();

// TODO: Specify your models here
module.exports = {
    Advert,
    City,
    Metro,
    sync: sequelize.sync.bind(this),
    close: () => sequelize.connectionManager.close(),
};

