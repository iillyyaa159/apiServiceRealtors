﻿const Sequelize = require('sequelize');
const AdvertModel = require('../../models/Advert');

module.exports = (sequelize) => {
    const Advert = sequelize.define('advert', {
        site_name:{
            type: Sequelize.TEXT,
        },
        url:{
            type: Sequelize.TEXT,
        },
        id_metro:{
            type: Sequelize.INTEGER,
        },
        seller_name:{
            type: Sequelize.STRING,
        },
        created:{
            type: Sequelize.DATE,
        },
        id_advert:{
            type: Sequelize.BIGINT,
        },
        content:{
            type: Sequelize.TEXT,
        },
        address:{
            type: Sequelize.STRING,
        },
        id_city:{
            type: Sequelize.INTEGER,
        },
        type:{
            type: Sequelize.STRING,
        },
        subtype:{
            type: Sequelize.STRING,
        },
        act:{
            type: Sequelize.STRING,
        },
        price:{
            type: Sequelize.DECIMAL,
        },
        phone:{
            type: Sequelize.STRING,
        },
        seller_type:{
            type: Sequelize.STRING,
        },
        phones_count:{
            type: Sequelize.INTEGER,
        },
        floor:{
            type: Sequelize.TINYINT,
        },
        floors:{
            type: Sequelize.TINYINT,
        },
        area_all:{
            type: Sequelize.FLOAT,
        },
        area_kitchen:{
            type: Sequelize.FLOAT,
        },
        area_live:{
            type: Sequelize.FLOAT,
        },
        rooms:{
            type: Sequelize.TINYINT,
        },
        photo_urls:{
            type: Sequelize.TEXT,
        },
        latitude:{
            type: Sequelize.DOUBLE,
        },
        longitude:{
            type: Sequelize.DOUBLE,
        },
        district:{
            type: Sequelize.STRING,
        },
        street:{
            type: Sequelize.STRING,
        },
        house:{
            type: Sequelize.STRING,
        }
    });

    Advert.prototype.toAdvertModel = function toAdvertModel() {
        return new AdvertModel(this.id, this.site_name, this.url, this.id_metro, this.seller_name, this.created,
            this.id_advert, this.content, this.address, this.id_city, this.type,
            this.subtype, this.act, this.price, this.phone, this.seller_type,
            this.phones_count, this.floor, this.floors, this.area_all, this.area_kitchen,
            this.area_live, this.rooms, this.photo_urls, this.latitude, this.longitude,
            this.district, this.street, this.house);
    };

    return Advert;
};

