﻿const Sequelize = require('sequelize');
const MetroModel = require('../../models/Metro');

module.exports = (sequelize) => {
    const Metro = sequelize.define('metro', {
        name:{
            type: Sequelize.STRING,
        }
    });

    Metro.prototype.toMetroModel = function toMetroModel() {
        return new MetroModel(this.id, this.name);
    };

    return Metro;
};

