﻿const Sequelize = require('sequelize');
const CityModel = require('../../models/City');

module.exports = (sequelize) => {
    const City = sequelize.define('city', {
        name:{
            type: Sequelize.STRING,
        }
    });

    City.prototype.toCityModel = function toCityModel() {
        return new CityModel(this.id, this.name);
    };

    return City;
};

