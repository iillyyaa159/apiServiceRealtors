function create(cityRepository) {
    async function getAll() {
        const citys = await cityRepository.getAll();
        return citys;
    }

    async function create(city) {
        await cityRepository.add(city);
    }

    async function upsert(city) {
        await cityRepository.upsert(city);
    }

    async function remove(city) {
        await cityRepository.remove(city);
    }

    return {
        create,
        getAll,
        upsert,
        remove,
    };
}

module.exports.create = create;

