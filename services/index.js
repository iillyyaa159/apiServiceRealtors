// Register all the services here
const advertServiceFactory = require('./advertService');
const metroServiceFactory = require('./metroService');
const cityServiceFactory = require('./cityService');

module.exports = (repositories) => {
    const advertService = advertServiceFactory.create(repositories.advertRepository);
    const metroService = metroServiceFactory.create(repositories.metroRepository);
    const cityService = cityServiceFactory.create(repositories.cityRepository);
    return ({
        advertService,
        metroService,
        cityService
    });
};
