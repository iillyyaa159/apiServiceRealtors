function create(advertRepository) {
    async function getAll() {
        const adverts = await advertRepository.getAll();
        return adverts;
    }

    async function get(id_advert) {
        const adverts = await advertRepository.get(id_advert);
        return adverts;
    }

    async function create(advert) {
        await advertRepository.add(advert);
    }

    async function upsert(advert) {
        await advertRepository.upsert(advert);
    }

    async function remove(advert) {
        await advertRepository.remove(advert);
    }

    return {
        create,
        getAll,
        upsert,
        remove,
        get,
    };
}

module.exports.create = create;

