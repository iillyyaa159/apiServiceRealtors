function create(metroRepository) {
    async function getAll() {
        const metros = await metroRepository.getAll();
        return metros;
    }

    async function create(metro) {
        await metroRepository.add(metro);
    }

    async function upsert(metro) {
        await metroRepository.upsert(metro);
    }

    async function remove(metro) {
        await metroRepository.remove(metro);
    }

    return {
        create,
        getAll,
        upsert,
        remove,
    };
}

module.exports.create = create;

