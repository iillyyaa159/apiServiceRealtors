const express = require('express');
const asyncWrapper = require('../utils/asyncWrapper');
const Adverts = require('../../models/Advert');
const Metro = require('../../models/Metro');
const City = require('../../models/City');
const fs = require("fs");
const multer = require('multer');
const upload = multer();
const Jimp = require("jimp");
const pach = require("../../pach");
const ip = require('ip');
const https = require(`https`);
const http = require(`http`);
const db = require('../../database');
const repositories = require('../../repositories')(db);
const services = require('../../services')(repositories);

let advertsArray = [];
const SLEEPTIME = 5 * 60 * 1000;

const router = express.Router();

function create(services) {
    // TODO: Install middleware to validate the input
    router.post('/', upload.array(), asyncWrapper(async (req, res) => {
        const adverts = JSON.parse(req.body.data);

        let countPars = 0;
        for (let item of adverts) {
            if (item.seller_type === "individual"
                && (item.site_name === "avito.ru" || item.site_name === "yandex.ru" || item.site_name === "cian.ru" ||  item.site_name === "youla.ru")) {
                advertsArray.push(item);

                countPars++;
            }
        }
        console.log(`Пришло всего объявлений ${adverts.length}, осталось для записи после фильтра ${countPars}`);
        res.sendStatus(200);
    }));

    return router;
}

async function addAdverts() {
    try {
        while (true) {
            try {
                let item = advertsArray[0];

                if (!item) {
                    await sleep(5000);
                    continue;
                }

                let advertsdb = [1];

                if (item) {
                    advertsdb = await services.advertService.get(item.id);
                }

                if (advertsdb.length === 0) {
                    console.time('Сохранение в бд и скачивание фотографий');
                    let advert = new Adverts();
                    advert.site_name = item.site_name;
                    advert.url = item.url;

                    console.time("Запись метро");
                    advert.id_metro = null;
                    if (item.metro !== null) {
                        let metro = await services.metroService.getAll();
                        let indexMetro = -1;
                        for (let me of metro) {
                            indexMetro = me.name.indexOf(item.metro);
                            if (indexMetro !== -1) {
                                indexMetro = me.id;
                                break;
                            }
                        }
                        if (indexMetro === -1) {
                            let metroItem = new Metro();
                            metroItem.name = item.metro;
                            await services.metroService.create(metroItem);
                            advert.id_metro = metro.length + 1;
                        }
                        else
                            advert.id_metro = indexMetro;
                    }
                    console.timeEnd('Запись метро');

                    advert.seller_name = item.seller_name;
                    advert.id_advert = item.id;
                    advert.content = item.content;
                    advert.address = item.address;

                    console.time("Запись города");
                    advert.id_city = null;
                    if (item.city !== null) {
                        let city = await services.cityService.getAll();
                        let indexCity = -1;
                        for (let ci of city) {
                            indexCity = ci.name.indexOf(item.city);
                            if (indexCity !== -1) {
                                indexCity = ci.id;
                                break;
                            }
                        }
                        if (indexCity === -1) {
                            let cityItem = new City();
                            cityItem.name = item.city;
                            await services.cityService.create(cityItem);
                            advert.id_city = city.length + 1;
                        }
                        else
                            advert.id_city = indexCity;
                    }
                    console.timeEnd("Запись города");

                    advert.type = item.type;
                    advert.subtype = item.subtype;
                    advert.act = item.act;
                    advert.price = item.price;
                    advert.phone = item.phone;
                    advert.seller_type = item.seller_type;
                    advert.phones_count = item.phones_count;
                    advert.floor = item.floor;
                    advert.floors = item.floors;
                    advert.area_all = item.area_all;
                    advert.area_kitchen = item.area_kitchen;
                    advert.area_live = item.area_live;
                    advert.rooms = item.rooms;

                    console.time("Получение пользователей и работа с ними");
                    let idUser = await services.advertService.getAll();
                    let idUserMax = idUser.length;
                    if (idUserMax !== 0) {
                        idUser = idUser[idUserMax - 1].id + 1;
                    }
                    else {
                        idUser = 1;
                    }
                    console.timeEnd("Получение пользователей и работа с ними");

                    let path = `./images/${idUser}`;

                    if (!fs.existsSync(`./images/`)) {
                        fs.mkdirSync(`./images/`);
                    }

                    console.time("Добавление фото в объявления");
                    if (item.photo_urls !== undefined)
                        for (let photo_url of item.photo_urls) {
                            advert.photo_urls += photo_url + ",";
                        }
                    console.timeEnd("Добавление фото в объявления");

                    advert.latitude = item.latitude;
                    advert.longitude = item.longitude;
                    advert.district = item.district;
                    advert.street = item.street;
                    advert.house = item.house;

                    console.time("Запись в базу");
                    await services.advertService.create(advert);
                    console.timeEnd("Запись в базу");

                    if (!fs.existsSync(path)) {
                        fs.mkdirSync(path);
                    }

                    console.time("Скачиваение фотографий");
                    let count = 1;
                    if (item.photo_urls !== undefined)
                        for (let photo_url of item.photo_urls) {
                            if (photo_url.indexOf("cian") > -1) {
                                await cropImage(photo_url, path + `/${count}.jpg`, 0);
                            }
                            else {
                                let file = fs.createWriteStream(path + `/${count}.jpg`);
                                try {
                                    if (photo_url.indexOf(`https`) > -1) {
                                        let request = https.get(photo_url, function (response) {
                                            response.pipe(file);
                                        });
                                    }
                                    else {
                                        let request = http.get(photo_url, function (response) {
                                            response.pipe(file);
                                        });
                                    }
                                }
                                catch (ex) {
                                    console.log(ex);
                                }
                            }
                            count++;
                        }
                    console.timeEnd("Скачиваение фотографий");
                    console.timeEnd('Сохранение в бд и скачивание фотографий');
                }

            }
            catch (ex) {
                console.log(ex);
            }
            advertsArray.splice(0, 1);
            console.log(`Осталось всего записать ${advertsArray.length}`);
            if (advertsArray.length === 0) {
                console.log('Очередь закончилась');
            }
        }
    }
    catch (ex) {
        Console.log(ex);
    }
}

async function sleep(sleepTime) {
    await new Promise(resolve => setTimeout(resolve, sleepTime));
}

async function cropImage(path, path2, cropCountHeight = 0) {
    try {
        await sleep(3000);
        await new Promise((resolve, reject) => {
            Jimp.read(path, function (err, lenna) {
                if (err){
                    console.log(err);
                    reject();
                }
                else {
                    try {
                        lenna.crop(0, 0, lenna.bitmap.width, (lenna.bitmap.height - (lenna.bitmap.height * cropCountHeight))).write(path2);
                        resolve();
                    } catch (ex) {
                        console.log(ex);
                        reject();
                    }
                }
            });
        });
    }
    catch (ex) {
        console.log(ex);
    }

}

module.exports = {
    create,
    addAdverts
};
