const express = require('express');
const bodyParser = require('body-parser');
const advertsRoute = require('./routes/adverts');
const errorRoute = require('./routes/error');

const app = express();
app.use(bodyParser.json());

module.exports = (services) => {
    const adverts = advertsRoute.create(services);

    app.use('/adverts', adverts);
    app.use(errorRoute);

    return app;
};
