// @flow

class Advert {
    constructor(id, site_name, url, id_metro, seller_name, created, id_advert, content, address, id_city, type, subtype, act, price, phone, seller_type,
                phones_count, floor, floors, area_all, area_kitchen, area_live, rooms, photo_urls, latitude, longitude, district, street, house) {
        this.id = id;
        this.site_name = site_name;
        this.url = url;
        this.id_metro = id_metro;
        this.seller_name = seller_name;
        this.created = created;
        this.id_advert = id_advert;
        this.content = content;
        this.address = address;
        this.id_city = id_city;
        this.type = type;
        this.subtype = subtype;
        this.act = act;
        this.price = price;
        this.phone = phone;
        this.seller_type = seller_type;
        this.phones_count = phones_count;
        this.floor = floor;
        this.floors = floors;
        this.area_all = area_all;
        this.area_kitchen = area_kitchen;
        this.area_live = area_live;
        this.rooms = rooms;
        this.photo_urls = photo_urls;
        this.latitude = latitude;
        this.longitude = longitude;
        this.district = district;
        this.street = street;
        this.house = house;
    }
}

module.exports = Advert;
